﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Library.DataContext;
namespace Library.Utils
{
    public static class Sequences
    {

        public static int GetAuhtorNextSequence()
        {
            return DataContext.DataContext.Authors().Max(a => a.ID)+1;
        }

        public static int GetBookNextSequence()
        {
            return DataContext.DataContext.Books().Max(b => b.ID)+1;
        }


    }
}