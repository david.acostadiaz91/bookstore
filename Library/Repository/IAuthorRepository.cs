﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Repository
{
    interface IAuthorRepository
    {
       
            IEnumerable<Authors> GetAll();
            Authors Get(int id);
            Authors Add(Authors author);
            void Remove(int id);
            bool Update(Authors author);


    }
}
