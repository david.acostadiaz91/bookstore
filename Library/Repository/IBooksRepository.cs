﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library.Models;
namespace Library.Repository
{
    interface IBooksRepository
    {
        IEnumerable<Books> GetAll();
        Books Get(int id);
        Books Add(Books book);
        void Remove(int id);
        bool Update(Books book);
    }
}
