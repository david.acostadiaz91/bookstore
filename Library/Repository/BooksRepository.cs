﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.Repository
{
    public class BooksRepository: IBooksRepository
    {
        private List<Books> books = new List<Books>();
        private int _nextId = 1;

        public BooksRepository()
        {
            Add(new Books { ID = 1, Title = "The Foundation", Description = "Asimov's book", PageCount = 0, Excerpt = "", PublishDate = "" });
            Add(new Books { ID = 2, Title = "20 mil leguas de viaje submarino", Description = "Verne's book", PageCount = 0, Excerpt = "", PublishDate = "" });
            Add(new Books { ID = 3, Title = "100 anios de soledad", Description = "García's book", PageCount = 0, Excerpt = "", PublishDate = "" });

        }



        public Books Add(Books book)
        {
            if (book == null)
            {
                throw new ArgumentNullException("item");
            }
            book.ID = _nextId++;
            books.Add(book);
            return book;
        }

        public Books Get(int id)
        {
            return books.Find(p => p.ID == id);
        }

        public IEnumerable<Books> GetAll()
        {
            return books;
        }

        public void Remove(int id)
        {
            books.RemoveAll(p => p.ID == id);
        }

        public bool Update(Books book)
        {
            if (book == null)
            {
                throw new ArgumentNullException("item");
            }
            int index = books.FindIndex(p => p.ID == book.ID);

            books.RemoveAt(index);
            books.Add(book);
            return true;
        }

    }
}