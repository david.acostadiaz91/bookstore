﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.Repository
{
    public class AuthorRepository : IAuthorRepository
    {

        private List<Authors> authors = new List<Authors>();
        //private int _nextId = 1;

        public AuthorRepository()
        {
            Add(new Authors { ID = 1, FirstName = "Isaac", LastName = "Asimov" });
            Add(new Authors { ID = 2, FirstName = "Jule", LastName = "Verne" });
            Add(new Authors { ID = 3, FirstName = "José Gabriel", LastName = "García Marquez" });
        }

        public Authors Add(Authors author)
        {
            if (author == null)
            {
                throw new ArgumentNullException("item");
            }
            author.ID = Utils.Sequences.GetAuhtorNextSequence()+1;
            authors.Add(author);
            return author;
        }

        public Authors Get(int id)
        {
            return authors.Find(p => p.ID == id);
        }

        public IEnumerable<Authors> GetAll()
        {
            return authors;
        }

        public void Remove(int id)
        {
            authors.RemoveAll(p => p.ID == id);
        }

        public bool Update(Authors author)
        {
            if (author == null)
            {
                throw new ArgumentNullException("item");
            }
            int index = authors.FindIndex(p => p.ID == author.ID);
          
            authors.RemoveAt(index);
            authors.Add(author);
            return true;
        }
    }
}