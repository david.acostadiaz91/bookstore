﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Library.DataContext
{
    public static class DataContext
    {

        public static List<Books> Books()
        {
            List<Books> book = new List<Books>
            {
                new Books { ID = 1, Title = "The Foundation", Description = "Asimov's book", PageCount=0, Excerpt="", PublishDate="" },
                new Books { ID = 2, Title = "20 mil leguas de viaje submarino", Description = "Verne's book", PageCount=0, Excerpt="", PublishDate=""},
                new Books { ID = 3, Title = "100 anios de soledad", Description = "García's book",PageCount=0,Excerpt="", PublishDate=""}
            };

            return book;
        }

        public static object BooksDetails2(int id)
        {
            var query = from b in Books()
                        where b.ID == id
                        join a in Authors() on b.ID equals a.ID
                        select new { b.Title, a.FirstName, a.LastName };

            return query;
        }

        public static List<BookDTO> BooksDetails(int id)
        {
            List<BookDTO> query = (from b in Books()
                        where b.ID == id
                        join a in Authors() on b.ID equals a.ID
                        select new BookDTO { Title= b.Title, 
                                             Description= b.Description ,
                                             AuthorName= a.FirstName+" "+a.LastName,
                                             PageCount = b.PageCount,
                                             PublishDate = b.PublishDate}).ToList();

            return query;
        }



        public static List<Authors> Authors()
        {
            List<Authors> authors = new List<Authors>
            {
                new Authors { ID = 1, FirstName = "Isaac", LastName = "Asimov"},
                new Authors { ID = 2, FirstName = "Jule", LastName = "Verne" },
                new Authors { ID = 3, FirstName = "José Gabriel", LastName = "García Marquez" }
            };

            return authors;
        }

    }
}