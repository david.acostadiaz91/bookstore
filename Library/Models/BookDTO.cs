﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Library.Models
{
    public class BookDTO
    {
        //[Display(Name = "Book ID")]
        //public int IDBook { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        [Display(Name = "Page Count")]
        public int PageCount { get; set; }
        public string Excerpt { get; set; }

        [Display(Name = "Publish Date")]
        public string PublishDate { get; set; }

        //[Display(Name = "Author Code")]
        //private int IDAuthor { get; set; }

        [Display(Name = "Author Name")]
        public string AuthorName { get; set; }





    }
}