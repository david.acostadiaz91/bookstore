﻿using Library.Models;
using Library.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
//using Library.DataContext;

namespace Library.Controllers
{
    public class BooksController : ApiController
    {

        static readonly IBooksRepository repository = new BooksRepository();


        public IEnumerable<Books> GetAllBooks()
        {
            return repository.GetAll();
        }

        public Books GetBook(int id)
        {
            Books book = repository.Get(id);
            if (book == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return book;
        }

        public Books PostBook(Books book)
        {
            book = repository.Add(book);
            return book;
        }

        public void PutBook(int id, Books book)
        {
            book.ID = id;
            if (!repository.Update(book))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        public void DeleteBook(int id)
        {
            Books item = repository.Get(id);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            repository.Remove(id);
        }

        //[HttpGet]
        //public IHttpActionResult GetBooks()
        //{
        //    return Json(DataContext.DataContext.Books());
        //}

        //[HttpGet]
        //public IHttpActionResult GetBook(int id)
        //{
        //    var _book = DataContext.DataContext.Books().FirstOrDefault(p => p.ID == id);
        //    if (_book == null)
        //    {
        //        return NotFound();
        //    }
        //    return Json(_book);
        //}

        //[HttpGet]
        //public IHttpActionResult GetBookDetail(int id)
        //{
        //    var _book = DataContext.DataContext.BooksDetails(id);
        //    if (_book == null)
        //    {
        //        return NotFound();
        //    }
        //    return Json(_book);
        //}


    }
}
