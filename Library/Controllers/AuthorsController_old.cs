﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using Library.Models;
using Newtonsoft.Json;

namespace Library.Controllers
{
    public class AuthorsController_old : ApiController
    {
       // static HttpClient client = new HttpClient();


        //string Baseurl = "https://fakerestapi.azurewebsites.net/";
        Authors[] authors = new Authors[]
       {
            new Authors { ID = 1, FirstName = "Isaac", LastName = "Asimov"},
            new Authors { ID = 2, FirstName = "Jule", LastName = "Verne" },
            new Authors { ID = 3, FirstName = "José Gabriel", LastName = "García Marquez" }
       };



        [HttpGet]
        public IEnumerable<Authors> GetAuthors()
        {
            return authors;
        }

        [HttpGet]
        public IHttpActionResult GetAuthor(int id)
        {
            var author = authors.FirstOrDefault(p => p.ID == id);
            if(author == null)
            {
                return NotFound();
            }
            return Ok(author);
        }








    }
}
