﻿using Library.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Library.Repository;
namespace Library.Controllers
{
    public class AuthorsController : ApiController
    {
        static readonly IAuthorRepository repository = new AuthorRepository();


        public IEnumerable<Authors> GetAllAuthors()
        {
            return repository.GetAll();
        }

        public Authors GetAuthor(int id)
        {
            Authors authors = repository.Get(id);
            if (authors == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
            return authors;
        }

        public Authors PostAuthor(Authors author)
        {
            author = repository.Add(author);
            return author;
        }

        public void PutAuthor(int id, Authors author)
        {
            author.ID = id;
            if (!repository.Update(author))
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }
        }

        public void DeleteAuthor(int id)
        {
            Authors item = repository.Get(id);
            if (item == null)
            {
                throw new HttpResponseException(HttpStatusCode.NotFound);
            }

            repository.Remove(id);
        }
        //public object Get()
        //{
        //    return Json(DataContext.DataContext.Authors());
        //}

        //[HttpGet]
        //public IHttpActionResult GetAuthors()
        //{
        //    return Json(DataContext.DataContext.Authors());
        //}

        //[HttpGet]
        //public IHttpActionResult GetAuthor(int id)
        //{
        //    var author = DataContext.DataContext.Authors().FirstOrDefault(p => p.ID == id);
        //    if (author == null)
        //    {
        //        return NotFound();
        //    }
        //    return Json(author);
        //}

        //[HttpPost]
        //public IHttpActionResult PostAuthor(string firstName, string lastName)
        //{

        //    if(string.IsNullOrEmpty(firstName))
        //    {
        //        return BadRequest();
        //    }  
        //    else
        //    {

        //        try
        //        {
        //            List<Authors> authors = new List<Authors>
        //            {
        //                 new Authors {ID = Utils.Sequences.GetAuhtorNextSequence(),  FirstName = firstName, LastName =lastName}
        //            };


        //            return Ok();
        //        }
        //        catch(Exception ex)
        //        {
        //            return BadRequest();
        //        }


        //    }

        //}



    }
}
