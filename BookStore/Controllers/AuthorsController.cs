﻿using BookStore.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace BookStore.Controllers
{
    public class AuthorsController : Controller
    {
        // GET: Authors
        //public ActionResult Index()
        //{
        //    return View();
        //}

        string Baseurl = "https://localhost:44356/";
        public async Task<ActionResult> Index()
        {
            List<Authors> authorsInfo = new List<Authors>();

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);

                client.DefaultRequestHeaders.Clear();
                
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage Res = await client.GetAsync("api/authors/GetAllAuthors");

                if (Res.IsSuccessStatusCode)
                {
                    var AuthorsResponse = Res.Content.ReadAsStringAsync().Result;

                    authorsInfo = JsonConvert.DeserializeObject<List<Authors>>(AuthorsResponse);

                }
                return View(authorsInfo);
            }


        }
        public ActionResult Create()
        {
            

            return View();
        }

        [HttpPost]
        public ActionResult Create(Authors author)
        {
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                client.DefaultRequestHeaders.Clear();

                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var json = JsonConvert.SerializeObject(author, Formatting.Indented);
                var auhtorInfo = new StringContent(json);

                HttpResponseMessage res = client.PostAsync("api/authors/PostAuthor", auhtorInfo).Result;
              
                if (res.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
            }

            ModelState.AddModelError(string.Empty, "Server Error. Please contact administrator.");

            return View(author);
        }

        public ActionResult Delete()
        {


            return View();
        }

        [HttpPost]
        public ActionResult Delete(int id)
        {
            Authors author = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(Baseurl);
                //HTTP GET
                var responseTask = client.GetAsync("api/authors/DeleteAuthor?id=" + id);
                responseTask.Wait();

                var result = responseTask.Result;
                if (result.IsSuccessStatusCode)
                {
                    var readTask = result.Content.ReadAsStringAsync().Result;
                   
                    

                     author = JsonConvert.DeserializeObject<Authors>(readTask);
                }
            }

            return View(author);
        }




    }
}